function sumOfPrimesUpto(number) {
  var sumOfPrimes = 0;
  var prime = 1;

  while(prime < number) {
    prime++;
    if(isPrime(prime)) {
      sumOfPrimes += prime;
    }
  }
  return sumOfPrimes;
}

function isPrime(prime) {
  if(prime<2) return false;
  else if(prime<3) return true;

  for(var i=2; i<Math.sqrt(prime)+1; i++) {
    if(prime%i==0) {
      return false;
    }
  }
  return true;
}

module.exports = sumOfPrimesUpto;