const assert = require('assert');
const sumOfPrimesUpto = require('../index.js');

describe('Problem9', function() {
  it('should give the correct sum for primes below 10', function() {
    assert.equal(sumOfPrimesUpto(10), 17);
  });
  it('should give the correct sum for primes below 2000000', function() {
    assert.equal(sumOfPrimesUpto(2000000), 142913828922)
  });
});
